﻿using System;

namespace LegacyCodeKata
{
    public class SecurityManager
    {
        private readonly IInputOutput _inputOutput;
        private readonly IEncryptionAlgorithm _encryptionAlgorithm;

        public SecurityManager(IInputOutput inputOutput, IEncryptionAlgorithm encryptionAlgorithm = null)
        {
            _inputOutput = inputOutput ?? throw new ArgumentNullException(nameof(inputOutput));
            _encryptionAlgorithm = encryptionAlgorithm ?? new ReverseEncryptionAlgorithm();
        }

        public void CreateUser()
        {
            _inputOutput.WriteLine("Enter a username");
            var username = _inputOutput.ReadLine();
            _inputOutput.WriteLine("Enter your full name");
            var fullName = _inputOutput.ReadLine();
            _inputOutput.WriteLine("Enter your password");
            var password = _inputOutput.ReadLine();
            _inputOutput.WriteLine("Re-enter your password");
            var confirmPassword = _inputOutput.ReadLine();

            if (password != confirmPassword)
            {
                _inputOutput.WriteLine("The passwords don't match");
                throw new PasswordsDontMatchException("The passwords don't match");
            }

            if (password.Length < 8)
            {
                _inputOutput.WriteLine("Password must be at least 8 characters in length");
                throw new PasswordsTooShortException("Password must be at least 8 characters in length");
            }

            var encryptedPassword = _encryptionAlgorithm.Encrypt(password);

            _inputOutput.Write($"Saving Details for User ({username}, {fullName}, {encryptedPassword})\n");
        }
    }
}