﻿using System;

namespace LegacyCodeKata
{
    public class ReverseEncryptionAlgorithm : IEncryptionAlgorithm
    {
        public string Encrypt(string text)
        {
            var array = text.ToCharArray();
            Array.Reverse(array);
            return new string(array);
        }
    }
}