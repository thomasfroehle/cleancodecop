﻿namespace LegacyCodeKata
{
    public interface IEncryptionAlgorithm
    {
        string Encrypt(string text);
    }
}