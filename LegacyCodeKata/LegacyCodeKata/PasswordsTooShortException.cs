﻿using System;

namespace LegacyCodeKata
{
    public class PasswordsTooShortException : Exception
    {
        public PasswordsTooShortException(string message) : base(message) { }
    }
}