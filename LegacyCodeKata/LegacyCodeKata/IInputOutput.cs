﻿namespace LegacyCodeKata
{
    public interface IInputOutput
    {
        string ReadLine();
        void WriteLine(string message);
        void Write(string message);
    }
}