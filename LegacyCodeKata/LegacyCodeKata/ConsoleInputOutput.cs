﻿using System;

namespace LegacyCodeKata
{
    public class ConsoleInputOutput : IInputOutput
    {
        public string ReadLine()
        {
            return Console.ReadLine();
        }

        public void WriteLine(string message)
        {
            Console.WriteLine(message);
        }

        public void Write(string message)
        {
            Console.Write(message);
        }
    }
}