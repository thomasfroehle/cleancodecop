﻿namespace LegacyCodeKata
{
    class Program
    {
        static void Main(string[] args)
        {
            var securityManager = new SecurityManager(new ConsoleInputOutput());
            securityManager.CreateUser();
        }
    }
}
