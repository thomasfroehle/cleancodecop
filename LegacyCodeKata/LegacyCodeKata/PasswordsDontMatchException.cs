﻿using System;

namespace LegacyCodeKata
{
    public class PasswordsDontMatchException : Exception
    {
        public PasswordsDontMatchException(string message) : base(message) { }
    }
}