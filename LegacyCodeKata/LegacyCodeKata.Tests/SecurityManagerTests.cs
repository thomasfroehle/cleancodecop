﻿using FakeItEasy;
using NUnit.Framework;

namespace LegacyCodeKata.Tests
{
    [TestFixture]
    public class SecurityManagerTests
    {

        private SecurityManager _sut;
        private IInputOutput _inputOutput;
        private IEncryptionAlgorithm _encryptionAlgorithm;

        [SetUp]
        public void SetUp()
        {
            _inputOutput = A.Fake<IInputOutput>(x => x.Strict());
            _encryptionAlgorithm = A.Fake<IEncryptionAlgorithm>(x => x.Strict());
            _sut = new SecurityManager(_inputOutput, _encryptionAlgorithm);
        }

        [Test]
        public void IfPasswordsMatchesAndIsLongerThan8UserIsSaved()
        {
            // Arrange
            A.CallTo(() => _inputOutput.WriteLine(A<string>._)).DoesNothing();
            A.CallTo(() => _inputOutput.Write(A<string>._)).DoesNothing();
            A.CallTo(() => _inputOutput.ReadLine()).ReturnsNextFromSequence("Hans", "Wurst", "secretPassword", "secretPassword");
            A.CallTo(() => _encryptionAlgorithm.Encrypt("secretPassword")).Returns("encryptedPassword");

            // Act
            _sut.CreateUser();

            // Arrange
            A.CallTo(() => _inputOutput.WriteLine(A<string>._)).MustHaveHappened(4, Times.Exactly);
            A.CallTo(() => _inputOutput.ReadLine()).MustHaveHappened(4, Times.Exactly);
            A.CallTo(() => _inputOutput.Write("Saving Details for User (Hans, Wurst, encryptedPassword)\n")).MustHaveHappenedOnceExactly();
        }

        [TestCase("passwort1", "passwort2")]
        [TestCase("", "passwort2")]
        [TestCase("passwort1", "")]
        [TestCase("passwort", "Passwort")]
        public void IfPasswordsDontMatchUserIsNotSaved(string password, string confirmPassword)
        {
            // Arrange
            A.CallTo(() => _inputOutput.WriteLine(A<string>._)).DoesNothing();
            A.CallTo(() => _inputOutput.Write(A<string>._)).DoesNothing();
            A.CallTo(() => _inputOutput.ReadLine()).ReturnsNextFromSequence("Hans", "Wurst", password, confirmPassword);

            // Act
            Assert.Throws<PasswordsDontMatchException>(() => _sut.CreateUser());

            // Arrange
            A.CallTo(() => _inputOutput.WriteLine("The passwords don't match")).MustHaveHappenedOnceExactly();
            A.CallTo(() => _inputOutput.Write(A<string>._)).MustNotHaveHappened();
        }


        [TestCase("", "")]
        [TestCase("pass", "pass")]
        [TestCase("1234567", "1234567")]
        public void IfPasswordsMatchButIsTooShortUserIsNotSaved(string password, string confirmPassword)
        {
            // Arrange
            A.CallTo(() => _inputOutput.WriteLine(A<string>._)).DoesNothing();
            A.CallTo(() => _inputOutput.Write(A<string>._)).DoesNothing();
            A.CallTo(() => _inputOutput.ReadLine()).ReturnsNextFromSequence("Hans", "Wurst", password, confirmPassword);

            // Act
            Assert.Throws<PasswordsTooShortException>(() => _sut.CreateUser());

            // Arrange
            A.CallTo(() => _inputOutput.WriteLine("Password must be at least 8 characters in length")).MustHaveHappenedOnceExactly();
            A.CallTo(() => _inputOutput.Write(A<string>._)).MustNotHaveHappened();
        }
    }
}
