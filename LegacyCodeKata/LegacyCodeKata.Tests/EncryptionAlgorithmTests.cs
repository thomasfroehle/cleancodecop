﻿using NUnit.Framework;

namespace LegacyCodeKata.Tests
{
    [TestFixture]
    public class EncryptionAlgorithmTests
    {
        [TestCase("password", "drowssap")]
        [TestCase("a", "a")]
        [TestCase("abc", "cba")]
        [TestCase("", "")]
        public void StringIsReversed(string password, string expectedResult)
        {
            // Arrange
            var sut = new ReverseEncryptionAlgorithm();

            // Act
            var result = sut.Encrypt(password);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }
    }
}
